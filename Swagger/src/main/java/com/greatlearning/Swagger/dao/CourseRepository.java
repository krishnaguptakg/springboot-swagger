package com.greatlearning.Swagger.dao;

import com.greatlearning.Swagger.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course,Integer> {
}
